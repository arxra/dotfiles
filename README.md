# DotFiles

Placed in `~/.config/`, these files configure my editor environment programs:

- [alacritty](https://github.com/alacritty/alacritty) - For when other terminals are to slow
- [bottom](https://github.com/ClementTsang/bottom) - htop is really 2003
- [fish](https://github.com/fish-shell/fish-shell) - Modern shell environment
- [nvim](https://github.com/neovim/neovim) - Modern development editor
- [sdkman](https://github.com/sdkman/sdkman-cli) - Java/Scala/Kotlin and so on virtual env manager
- [starship](https://github.com/starship/starship) - Easy to install and useful prompt for your shell.
- [wezterm](https://github.com/wez/wezterm) - Not as fast as alacritty, but prettier and more features
- [zellij](https://github.com/zellij-org/zellij) - Tmux through oh-my-tmux is a solid alternative, zellij is there any day now for usability.
- [zoxide](https://github.com/ajeetdsouza/zoxide) - Smarter `cd` command, shorted to `z`

note: Not all of these have config files but rather are listed as a guide.

To get started quickly:

```bash
cd
mv .config .config.bak
git clone git@gitlab.com:arxra/dotfiles.git ~/.config
```

Then open and close Neovim after it finishes loading (this should install all plugins and tree-sitter syntax modules)

## History

Neovim config used to be based on [LunarVim](https://github.com/LunarVim/LunarVim) and placed in the `lvim` directory.
This is no longer used.

Terminal might be alacritty or wezterm, haven't decided yet.
