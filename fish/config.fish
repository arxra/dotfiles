if status is-interactive
    # Commands to run in interactive sessions can go here
end

# NVM loading
export NVM_DIR="$HOME/.nvm"
export EDITOR="nvim"
# Pronpts
starship init fish | source
zoxide init fish | source

# Kanagawa Fish shell theme
# A template was taken and modified from Tokyonight:
# https://github.com/folke/tokyonight.nvim/blob/main/extras/fish_tokyonight_night.fish
set -l foreground DCD7BA
set -l selection 2D4F67
set -l comment 727169
set -l red C34043
set -l orange FF9E64
set -l yellow C0A36E
set -l green 76946A
set -l purple 957FB8
set -l cyan 7AA89F
set -l pink D27E99

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment

# Created by `pipx` on 2022-08-09 12:09:46
set PATH $PATH /Users/jarhb/.local/bin

### MANAGED BY RANCHER DESKTOP START (DO NOT EDIT)
set --export --prepend PATH "/Users/aronhansenberggren/.rd/bin"
### MANAGED BY RANCHER DESKTOP END (DO NOT EDIT)

# Added by LM Studio CLI (lms)
set -gx PATH $PATH /Users/aronhansenberggren/.lmstudio/bin
set PATH $HOME/.jenv/bin $PATH
status --is-interactive; and jenv init - | source
set PATH $HOME/.jenv/bin $PATH
status --is-interactive; and jenv init - | source
set PATH $HOME/.jenv/bin $PATH
status --is-interactive; and jenv init - | source
