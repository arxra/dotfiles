-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
--
-- Diagnostic settings
vim.diagnostic.config({
  virtual_text = false,
  signs = true,
  underline = true,
  severity_sort = true,
})

vim.opt.colorcolumn = "80"
vim.opt.relativenumber = false
vim.opt.laststatus = 3
vim.g.lazyvim_python_lsp = "basedpyright"
vim.g.snacks_animate = false
