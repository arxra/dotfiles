return {
  "yetone/avante.nvim",
  event = "VeryLazy",
  lazy = false,
  version = false, -- Set this to "*" to always pull the latest release version, or set it to false to update to the latest code changes.
  opts = {
    provider = "openai",
    auto_suggestions_provider = "openai",
    openai = {
      endpoint = "http://127.0.0.1:1234",
      model = "qwen2:7b",
      temperature = 0,
      max_tokens = 4096,
      ["local"] = true,
    },
  },
}
