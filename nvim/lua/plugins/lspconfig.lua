return {
  {
    "neovim/nvim-lspconfig",
    opts = function(_, opts)
      -- add tailwind to rust files
      -- opts.servers.tailwindcss = require("lspconfig.server_configurations.tailwindcss").default_config
      -- table.insert(opts.servers.tailwindcss.filetypes, "rust")
      -- opts.servers.tailwindcss.init_options.userLanguages.rust = "html"
      -- opts.servers.emmet_ls = require("lspconfig.server_configurations.emmet_ls").default_config
      -- table.insert(opts.servers.emmet_ls.filetypes, "rust")
    end,

    init = function()
      -- local keys = require("lazyvim.plugins.lsp.keymaps").get()
      -- -- disable a keymap
      -- keys[#keys + 1] = { "K", false }
      -- keys[#keys + 1] = { "gr", false }
      -- keys[#keys + 1] = { "gR", false }
      -- keys[#keys + 1] = { "gD", false }
    end,
  },
}
