return {
  {
    "iamcco/markdown-preview.nvim",
    ft = "markdown",
    init = function()
      vim.g.mkdp_filetypes = { "markdown" }
    end,
    build = ":call mkdp#util#install()",
    cmd = { "MarkdownPreview", "MarkdownPreviewToggle", "MarkdownPreviewStop" },
  },
}
