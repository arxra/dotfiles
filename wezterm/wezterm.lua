local wezterm = require("wezterm")
local act = wezterm.action
require("status")

function scheme_for_appearance(appearance)
	if appearance:find("Dark") then
		return "Catppuccin Macchiato"
	else
		return "Catppuccin Latte"
	end
end

return {
	color_scheme = scheme_for_appearance(wezterm.gui.get_appearance()),
	window_decorations = "NONE",
	font = wezterm.font({ family = "Iosevka Nerd Font" }),
	-- window_background_opacity = 0.85,
	dpi_by_screen = {
		["XF270HU"] = 80,
		["C34H89x"] = 76,
		["DELL U3421WE"] = 78,
	},
	freetype_load_flags = "NO_HINTING",
	-- hide_tab_bar_if_only_one_tab = true,
	enable_tab_bar = false,
	send_composed_key_when_right_alt_is_pressed = true,
	window_close_confirmation = "NeverPrompt",

	mouse_bindings = {
		-- Change the default click behavior so that it populates
		-- the Clipboard rather the PrimarySelection.
		{
			event = { Up = { streak = 1, button = "Left" } },
			mods = "NONE",
			action = act.CompleteSelectionOrOpenLinkAtMouseCursor("Clipboard"),
		},
	},
	disable_default_key_bindings = true,
	keys = {
		{
			key = " ",
			mods = "SHIFT|CTRL",
			action = act.ToggleFullScreen,
		},
		-- { key = "/", mods = "SHIFT|ALT", action = act.SendString("\\") },
		{ key = "v", mods = "SUPER", action = act.PasteFrom("Clipboard") },
		{ key = "c", mods = "SUPER", action = act.CopyTo("Clipboard") },
		{ key = "v", mods = "CTRL|SHIFT", action = act.PasteFrom("Clipboard") },
		{ key = "c", mods = "CTRL|SHIFT", action = act.CopyTo("Clipboard") },
		{ key = "l", mods = "CTRL|SHIFT", action = act.ShowDebugOverlay },
		{ key = "U", mods = "SHIFT|SUPER", action = act.CharSelect },
		{ key = "-", mods = "SUPER", action = act.DecreaseFontSize },
		{ key = "+", mods = "SUPER", action = act.IncreaseFontSize },
		{ key = "=", mods = "CTRL|SHIFT", action = act.ResetFontSize },
		{ key = "P", mods = "SHIFT|SUPER", action = act.ActivateCommandPalette },
		-- { key = "w", mods = "SUPER", action = act.CloseCurrentTab({ confirm = true }) },
		-- { key = "t", mods = "CTRL|SHIFT", action = act.SpawnTab("CurrentPaneDomain") },
		-- { key = "1", mods = "CTRL|SHIFT", action = act.ActivateTab(0) },
		-- { key = "2", mods = "CTRL|SHIFT", action = act.ActivateTab(1) },
		-- { key = "3", mods = "CTRL|SHIFT", action = act.ActivateTab(2) },
		-- { key = "4", mods = "CTRL|SHIFT", action = act.ActivateTab(3) },
		-- { key = "5", mods = "CTRL|SHIFT", action = act.ActivateTab(4) },
	},
	window_frame = {
		-- Berkeley Mono for me again, though an idea could be to try a
		-- serif font here instead of monospace for a nicer look?
		font = wezterm.font({ family = "FiraCode Nerd Font", weight = "Bold" }),
		font_size = 11,
	},
	window_padding = {
		left = 0,
		right = 0,
		top = 0,
		bottom = 0,
	},
}
